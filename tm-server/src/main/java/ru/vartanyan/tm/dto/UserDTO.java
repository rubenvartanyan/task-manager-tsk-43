package ru.vartanyan.tm.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.vartanyan.tm.enumerated.Role;

import javax.persistence.*;

@Setter
@Getter
@NoArgsConstructor
@Entity
@Table(name = "app_user")
public class UserDTO extends AbstractEntityDTO {

    @Column
    @Nullable private String login;

    @Column(name = "password_hash")
    @Nullable private String passwordHash;

    @Column
    @Nullable private String email;

    @Column(name = "first_name")
    @Nullable private String firstName;

    @Column(name = "last_name")
    @Nullable private String lastName;

    @Column(name = "middle_name")
    @Nullable private String middleName;

    @Enumerated(EnumType.STRING)
    @NotNull private Role role = Role.USER;

    @Column
    @NotNull private Boolean locked = false;

    @Override
    public String toString() {
        return "User{" +
                "login='" + login + '\'' +
                ", passwordHash='" + passwordHash + '\'' +
                ", email='" + email + '\'' +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", middleName='" + middleName + '\'' +
                ", role=" + role +
                ", role=" + id +
                ", locked=" + locked +
                '}';
    }

}
