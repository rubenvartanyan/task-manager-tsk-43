package ru.vartanyan.tm.repository.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.vartanyan.tm.api.repository.model.ITaskRepository;
import ru.vartanyan.tm.model.Task;

import javax.persistence.EntityManager;
import java.util.List;
import java.util.Optional;

public class TaskRepository extends AbstractRepository<Task> implements ITaskRepository {

    public TaskRepository(@NotNull EntityManager entityManager) {
        super(entityManager);
    }

    public @NotNull Task findOneById(@Nullable final String id) {
        return entityManager.find(Task.class, id);
    }

    public void remove(final Task entity) {
        Task reference = entityManager.getReference(Task.class, entity.getId());
        entityManager.remove(reference);
    }

    public void removeOneById(@Nullable final String id) {
        Task reference = entityManager.getReference(Task.class, id);
        entityManager.remove(reference);
    }

    @NotNull
    public List<Task> findAll() {
        return entityManager.createQuery("SELECT e FROM Task e", Task.class).getResultList();
    }

    @NotNull
    @Override
    public List<Task> findAllByUserId(String userId) {
        return entityManager
                .createQuery("SELECT e FROM Task e WHERE e.user.id = :userId", Task.class)
                .setParameter("userId", userId)
                .getResultList();
    }

    @NotNull
    @Override
    public List<Task> findAllByProjectId(@Nullable final String userId, @Nullable final String projectId) {
        return entityManager
                .createQuery(
                        "SELECT e FROM Task e WHERE e.user.id = :userId AND e.project.id = :projectId",
                        Task.class
                )
                .setParameter("userId", userId)
                .setParameter("projectId", projectId)
                .getResultList();
    }

    @Override
    public void removeAllByProjectId(
            @Nullable final String userId, @Nullable final String projectId
    ) {
        entityManager
                .createQuery(
                        "DELETE FROM Task e WHERE e.user.id = :userId AND e.project.id = :projectId"
                )
                .setParameter("userId", userId)
                .setParameter("projectId", projectId)
                .executeUpdate();
    }

    @Override
    public void bindTaskByProjectId(
            @NotNull final String userId,
            @NotNull final String projectId,
            @NotNull final String taskId
    ) {
        entityManager
                .createQuery(
                        "UPDATE Task e SET e.project.id = :projectId WHERE e.user.id = :userId AND e.id = :id"
                )
                .setParameter("userId", userId)
                .setParameter("id", taskId)
                .setParameter("projectId", projectId)
                .executeUpdate();
    }

    @Override
    public void unbindTaskFromProjectId(@NotNull final String userId, @NotNull final String id) {
        entityManager
                .createQuery(
                        "UPDATE Task e SET e.project.id = NULL WHERE e.user.id = :userId AND e.id = :id"
                )
                .setParameter("userId", userId)
                .setParameter("id", id)
                .executeUpdate();
    }

    @Override
    public @NotNull Task findOneByIdAndUserId(
            @Nullable final String userId, @Nullable final String id
    ) {
        return getSingleResult(entityManager
                .createQuery(
                        "SELECT e FROM Task e WHERE e.id = :id AND e.user.id = :userId", Task.class
                )
                .setParameter("id", id)
                .setParameter("userId", userId)
                .setMaxResults(1));
    }

    @Override
    public @NotNull Task findOneByName(
            @Nullable final String userId, @Nullable final String name
    ) {
        List<Task> list = entityManager
                .createQuery(
                        "SELECT e FROM Task e WHERE e.name = :name AND e.user.id = :userId",
                        Task.class
                )
                .setParameter("name", name)
                .setParameter("userId", userId)
                .setMaxResults(1).getResultList();

        return list.get(0);
    }

    @Override
    public @NotNull Task findOneByIndex(
            @Nullable final String userId, @Nullable final Integer index
    ) {
        List<Task> list = entityManager
                .createQuery("SELECT e FROM Task e WHERE e.user.id = :userId", Task.class)
                .setParameter("userId", userId)
                .setFirstResult(index).setMaxResults(1).getResultList();

        return list.get(0);
    }

    @Override
    public void removeOneByName(@Nullable final String userId, @Nullable final String name) {
        entityManager
                .createQuery("DELETE FROM Task e WHERE e.name = :name AND e.user.id = :userId")
                .setParameter("userId", userId)
                .setParameter("name", name)
                .executeUpdate();
    }

    public void clear() {
        entityManager
                .createQuery("DELETE FROM Task e")
                .executeUpdate();
    }

    @Override
    public void clearByUserId(@Nullable final String userId) {
        entityManager
                .createQuery("DELETE FROM Task e WHERE e.userId = :userId")
                .setParameter("userId", userId).executeUpdate();
    }

    @Override
    public void removeOneByIdAndUserId(@Nullable final String userId, @Nullable final String id) {
        entityManager
                .createQuery("DELETE FROM Task e WHERE e.user.id = :userId AND e.id =:id")
                .setParameter("id", id)
                .setParameter("userId", userId)
                .executeUpdate();
    }


}
