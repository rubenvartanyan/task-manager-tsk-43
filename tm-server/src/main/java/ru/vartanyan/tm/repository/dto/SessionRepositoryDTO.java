package ru.vartanyan.tm.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.vartanyan.tm.api.repository.dto.ISessionRepositoryDTO;
import ru.vartanyan.tm.dto.SessionDTO;

import javax.persistence.EntityManager;
import java.util.List;
import java.util.Optional;

public class SessionRepositoryDTO extends AbstractRepositoryDTO<SessionDTO> implements ISessionRepositoryDTO {

    public SessionRepositoryDTO(@NotNull EntityManager entityManager) {
        super(entityManager);
    }

    @NotNull
    public List<SessionDTO> findAll() {
        return entityManager.createQuery("SELECT e FROM SessionDTO e", SessionDTO.class).getResultList();
    }

    public @NotNull SessionDTO findOneById(@Nullable final String id) {
        return entityManager.find(SessionDTO.class, id);
    }

    public void clear() {
        entityManager
                .createQuery("DELETE FROM SessionDTO e")
                .executeUpdate();
    }

    public void removeOneById(@Nullable final String id) {
        SessionDTO reference = entityManager.getReference(SessionDTO.class, id);
        entityManager.remove(reference);
    }

}

