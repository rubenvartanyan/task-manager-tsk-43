package ru.vartanyan.tm.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.vartanyan.tm.api.repository.IRepositoryDTO;
import ru.vartanyan.tm.dto.AbstractEntityDTO;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import java.util.List;
import java.util.Optional;

public abstract class AbstractRepositoryDTO<E extends AbstractEntityDTO> implements IRepositoryDTO<E> {

    @NotNull
    protected final EntityManager entityManager;

    protected AbstractRepositoryDTO(@NotNull final EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    public void add(@Nullable final E entity) {
        if (entity == null) return;
        entityManager.persist(entity);
    }

    @Override
    public void update(@Nullable final E entity) {
        if (entity == null) return;
        entityManager.merge(entity);
    }

    @NotNull
    @Override
    public E getSingleResult(@NotNull final TypedQuery<E> query) {
        @NotNull final List<E> list = query.getResultList();
        return list.get(0);
    }

}

