package ru.vartanyan.tm.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.vartanyan.tm.enumerated.Status;

import javax.persistence.*;
import java.util.Date;

@Getter
@Setter
@NoArgsConstructor
@MappedSuperclass
public abstract class AbstractBusinessEntity extends AbstractEntity{

    @Column
    @NotNull
    public String name = "";

    @Column
    @NotNull public String description = "";

    @Nullable
    @ManyToOne
    private User user;

    @Enumerated(EnumType.STRING)
    @NotNull public Status status = Status.NOT_STARTED;

    @Column(name = "date_started")
    @Nullable
    public Date dateStarted;

    @Column(name = "date_finish")
    @Nullable public Date dateFinish;

    @Column
    @NotNull public Date created = new Date();

}
