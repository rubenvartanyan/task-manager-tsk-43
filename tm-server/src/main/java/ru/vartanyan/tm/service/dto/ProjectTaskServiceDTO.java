package ru.vartanyan.tm.service.dto;

import lombok.SneakyThrows;
import org.apache.ibatis.session.SqlSession;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.vartanyan.tm.api.repository.dto.IProjectRepositoryDTO;
import ru.vartanyan.tm.api.repository.dto.ITaskRepositoryDTO;
import ru.vartanyan.tm.api.repository.model.IProjectRepository;
import ru.vartanyan.tm.api.repository.model.ITaskRepository;
import ru.vartanyan.tm.api.service.IConnectionService;
import ru.vartanyan.tm.api.service.dto.IProjectTaskServiceDTO;
import ru.vartanyan.tm.api.service.model.IProjectTaskService;
import ru.vartanyan.tm.dto.TaskDTO;
import ru.vartanyan.tm.exception.empty.EmptyIdException;
import ru.vartanyan.tm.repository.dto.ProjectRepositoryDTO;
import ru.vartanyan.tm.repository.dto.TaskRepositoryDTO;

import javax.persistence.EntityManager;
import java.util.List;

public final class ProjectTaskServiceDTO implements IProjectTaskServiceDTO {

    @NotNull
    private final IConnectionService connectionService;

    public ProjectTaskServiceDTO(@NotNull IConnectionService connectionService) {
        this.connectionService = connectionService;
    }

    @SneakyThrows
    @Override
    public void bindTaskByProject(
            @Nullable final String userId,
            @Nullable final String projectId,
            @Nullable final String taskId
    ) {
        if (userId == null) throw new EmptyIdException();
        if (projectId == null) throw new EmptyIdException();
        if (taskId == null) throw new EmptyIdException();
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            @NotNull final ITaskRepositoryDTO taskRepository = new TaskRepositoryDTO(entityManager);
            taskRepository.bindTaskByProjectId(userId, projectId, taskId);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @NotNull
    @SneakyThrows
    @Override
    public List<TaskDTO> findAllByProjectId(
            @Nullable final String userId,
            @Nullable final String projectId
    ) {
        if (userId == null) throw new EmptyIdException();
        if (projectId == null) throw new EmptyIdException();
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        @NotNull final ITaskRepositoryDTO taskRepository = new TaskRepositoryDTO(entityManager);
        return taskRepository.findAllByProjectId(userId, projectId);
    }

    @SneakyThrows
    @Override
    public void removeProjectById(
            @Nullable final String userId,
            @Nullable final String projectId
    ) {
        if (userId == null) throw new EmptyIdException();
        if (projectId == null) throw new EmptyIdException();
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            @NotNull final ITaskRepositoryDTO taskRepository = new TaskRepositoryDTO(entityManager);
            @NotNull final IProjectRepositoryDTO projectRepository = new ProjectRepositoryDTO(entityManager);
            taskRepository.removeAllByProjectId(userId, projectId);
            projectRepository.removeOneByIdAndUserId(userId, projectId);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @SneakyThrows
    @Override
    public void unbindTaskFromProject(
            @Nullable final String userId, @Nullable final String taskId
    ) {
        if (userId == null) throw new EmptyIdException();
        if (taskId == null) throw new EmptyIdException();
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            @NotNull final ITaskRepositoryDTO taskRepository = new TaskRepositoryDTO(entityManager);
            taskRepository.unbindTaskFromProjectId(userId, taskId);
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

}
