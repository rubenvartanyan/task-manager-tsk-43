package ru.vartanyan.tm.service.model;

import org.jetbrains.annotations.NotNull;
import ru.vartanyan.tm.api.service.model.IService;
import ru.vartanyan.tm.api.service.IConnectionService;
import ru.vartanyan.tm.model.AbstractEntity;

public abstract class AbstractService<E extends AbstractEntity> implements IService<E> {

    @NotNull
    public final IConnectionService connectionService;

    public AbstractService(@NotNull IConnectionService connectionService) {
        this.connectionService = connectionService;
    }

}
