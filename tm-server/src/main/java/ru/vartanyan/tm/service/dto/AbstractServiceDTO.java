package ru.vartanyan.tm.service.dto;

import org.jetbrains.annotations.NotNull;
import ru.vartanyan.tm.api.service.IConnectionService;
import ru.vartanyan.tm.api.service.dto.IServiceDTO;
import ru.vartanyan.tm.dto.AbstractEntityDTO;

public abstract class AbstractServiceDTO<E extends AbstractEntityDTO> implements IServiceDTO<E> {

    @NotNull
    public final IConnectionService connectionService;

    public AbstractServiceDTO(@NotNull IConnectionService connectionService) {
        this.connectionService = connectionService;
    }

}
