package ru.vartanyan.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.vartanyan.tm.api.endpoint.ISessionEndpoint;
import ru.vartanyan.tm.api.service.ServiceLocator;
import ru.vartanyan.tm.dto.SessionDTO;
import ru.vartanyan.tm.dto.UserDTO;
import ru.vartanyan.tm.exception.system.AccessDeniedException;
import ru.vartanyan.tm.exception.system.UserLockedException;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@WebService
public final class SessionEndpoint extends AbstractEndpoint implements ISessionEndpoint {

    public SessionEndpoint() {
        super(null);
    }

    public SessionEndpoint(final ServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @Override
    @WebMethod
    public SessionDTO openSession(
            @WebParam(name = "login", partName = "login") @Nullable final String login,
            @WebParam(name = "password", partName = "password") @Nullable final String password
    ) throws UserLockedException, AccessDeniedException {
        return serviceLocator.getSessionServiceDTO().open(login, password);
    }

    @Override
    @Nullable
    @WebMethod
    public SessionDTO closeSession(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO session
    ) throws AccessDeniedException {
        serviceLocator.getSessionServiceDTO().validate(session);
        return serviceLocator.getSessionServiceDTO().close(session);
    }

}
