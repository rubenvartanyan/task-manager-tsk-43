package ru.vartanyan.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.vartanyan.tm.api.endpoint.IProjectEndpoint;
import ru.vartanyan.tm.api.service.ServiceLocator;
import ru.vartanyan.tm.dto.ProjectDTO;
import ru.vartanyan.tm.dto.SessionDTO;
import ru.vartanyan.tm.exception.system.AccessDeniedException;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@WebService
public final class ProjectEndpoint extends AbstractEndpoint implements IProjectEndpoint {

    public ProjectEndpoint(@NotNull final ServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @Override
    @WebMethod
    public ProjectDTO addProject(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO session,
            @WebParam(name = "name", partName = "name") @NotNull final String name,
            @WebParam(name = "description", partName = "description") @NotNull final String description
    ) throws AccessDeniedException {
        serviceLocator.getSessionServiceDTO().validate(session);
        if (session == null) throw new AccessDeniedException();
        return serviceLocator.getProjectServiceDTO().add(session.getUserId(), name, description);
    }

    @Override
    @WebMethod
    public void clearBySessionProject(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO session
    ) throws AccessDeniedException{
        serviceLocator.getSessionServiceDTO().validate(session);
        if (session == null) throw new AccessDeniedException();
        if (session.getUserId() == null) throw new AccessDeniedException();
        serviceLocator.getProjectServiceDTO().clear(session.getUserId());
    }

    @Override
    @NotNull
    @WebMethod
    public List<ProjectDTO> findProjectAll(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO session
    ) throws AccessDeniedException{
        serviceLocator.getSessionServiceDTO().validate(session);
        if (session == null) throw new AccessDeniedException();
        return serviceLocator.getProjectServiceDTO().findAll(session.getUserId());
    }

    @Override
    @Nullable
    @WebMethod
    public ProjectDTO findProjectOneById(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO session,
            @WebParam(name = "id", partName = "id") @NotNull final String id
    ) throws AccessDeniedException {
        serviceLocator.getSessionServiceDTO().validate(session);
        if (session == null) throw new AccessDeniedException();
        return serviceLocator.getProjectServiceDTO().findOneById(session.getUserId(), id);
    }

    @Override
    @Nullable
    @WebMethod
    public ProjectDTO findProjectOneByIndex(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO session,
            @WebParam(name = "index", partName = "index") @NotNull final Integer index
    ) throws AccessDeniedException{
        serviceLocator.getSessionServiceDTO().validate(session);
        if (session == null) throw new AccessDeniedException();
        return serviceLocator.getProjectServiceDTO().findOneByIndex(session.getUserId(), index);
    }

    @Override
    @Nullable
    @WebMethod
    public ProjectDTO findProjectOneByName(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO session,
            @WebParam(name = "name", partName = "name") @NotNull final String name
    ) throws AccessDeniedException{
        serviceLocator.getSessionServiceDTO().validate(session);
        if (session == null) throw new AccessDeniedException();
        return serviceLocator.getProjectServiceDTO().findOneByName(session.getUserId(), name);
    }

    @Override
    @WebMethod
    public void finishProjectById(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO session,
            @WebParam(name = "id", partName = "id") @NotNull final String id
    ) throws AccessDeniedException{
        serviceLocator.getSessionServiceDTO().validate(session);
        if (session == null) throw new AccessDeniedException();
        serviceLocator.getProjectServiceDTO().finishById(session.getUserId(), id);
    }

    @Override
    @WebMethod
    public void finishProjectByIndex(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO session,
            @WebParam(name = "index", partName = "index") @NotNull final Integer index
    ) throws AccessDeniedException{
        serviceLocator.getSessionServiceDTO().validate(session);
        if (session == null) throw new AccessDeniedException();
        serviceLocator.getProjectServiceDTO().finishByIndex(session.getUserId(), index);
    }

    @Override
    @WebMethod
    public void finishProjectByName(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO session,
            @WebParam(name = "name", partName = "name") @NotNull final String name
    ) throws AccessDeniedException{
        serviceLocator.getSessionServiceDTO().validate(session);
        if (session == null) throw new AccessDeniedException();
        serviceLocator.getProjectServiceDTO().finishByName(session.getUserId(), name);
    }

    @Override
    @WebMethod
    public void removeProject(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO session,
            @WebParam(name = "project", partName = "project") @NotNull ProjectDTO project
    ) throws AccessDeniedException{
        serviceLocator.getSessionServiceDTO().validate(session);
        if (session == null) throw new AccessDeniedException();
        serviceLocator.getProjectServiceDTO().remove(session.getUserId(), project);
    }

    @Override
    @WebMethod
    public void removeProjectOneById(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO session,
            @WebParam(name = "id", partName = "id") @NotNull final String id
    ) throws AccessDeniedException{
        serviceLocator.getSessionServiceDTO().validate(session);
        if (session == null) throw new AccessDeniedException();
        serviceLocator.getProjectServiceDTO().removeOneById(session.getUserId(), id);
    }

    @Override
    @WebMethod
    public void removeProjectOneByIndex(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO session,
            @WebParam(name = "index", partName = "index") @NotNull final Integer index
    ) throws AccessDeniedException{
        serviceLocator.getSessionServiceDTO().validate(session);
        if (session == null) throw new AccessDeniedException();
        serviceLocator.getProjectServiceDTO().removeOneByIndex(session.getUserId(), index);
    }

    @Override
    @WebMethod
    public void removeProjectOneByName(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO session,
            @WebParam(name = "name", partName = "name") @NotNull final String name
    ) throws AccessDeniedException{
        serviceLocator.getSessionServiceDTO().validate(session);
        if (session == null) throw new AccessDeniedException();
        serviceLocator.getProjectServiceDTO().removeOneByName(session.getUserId(), name);
    }

    @Override
    @WebMethod
    public void removeProjectByIdWithTask(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO session,
            @WebParam(name = "projectId", partName = "projectId") @Nullable final String projectId
    ) throws AccessDeniedException{
        serviceLocator.getSessionServiceDTO().validate(session);
        if (session == null) throw new AccessDeniedException();
        serviceLocator.getProjectTaskServiceDTO().removeProjectById(session.getUserId(), projectId);
    }

    @Override
    @WebMethod
    public void startProjectById(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO session,
            @WebParam(name = "id", partName = "id") @NotNull final String id
    ) throws AccessDeniedException{
        serviceLocator.getSessionServiceDTO().validate(session);
        if (session == null) throw new AccessDeniedException();
        serviceLocator.getProjectServiceDTO().startById(session.getUserId(), id);
    }

    @Override
    @WebMethod
    public void startProjectByIndex(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO session,
            @WebParam(name = "index", partName = "index") @NotNull final Integer index
    ) throws AccessDeniedException{
        serviceLocator.getSessionServiceDTO().validate(session);
        if (session == null) throw new AccessDeniedException();
        serviceLocator.getProjectServiceDTO().startByIndex(session.getUserId(), index);
    }

    @Override
    @WebMethod
    public void startProjectByName(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO session,
            @WebParam(name = "name", partName = "name") @NotNull final String name
    ) throws AccessDeniedException{
        serviceLocator.getSessionServiceDTO().validate(session);
        if (session == null) throw new AccessDeniedException();
        serviceLocator.getProjectServiceDTO().startByName(session.getUserId(), name);
    }

    @Override
    @WebMethod
    public void updateProjectById(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO session,
            @WebParam(name = "id", partName = "id") @NotNull final String id,
            @WebParam(name = "name", partName = "name") @NotNull final String name,
            @WebParam(name = "description", partName = "description") @NotNull final String description
    ) throws AccessDeniedException{
        serviceLocator.getSessionServiceDTO().validate(session);
        if (session == null) throw new AccessDeniedException();
        serviceLocator.getProjectServiceDTO().updateById(session.getUserId(), id, name, description);
    }

    @Override
    @Nullable
    @WebMethod
    public void updateProjectByIndex(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO session,
            @WebParam(name = "index", partName = "index") @NotNull final Integer index,
            @WebParam(name = "name", partName = "name") @NotNull final String name,
            @WebParam(name = "description", partName = "description") @NotNull final String description
    ) throws AccessDeniedException{
        serviceLocator.getSessionServiceDTO().validate(session);
        if (session == null) throw new AccessDeniedException();
        serviceLocator.getProjectServiceDTO().updateByIndex(session.getUserId(), index, name, description);
    }

}
