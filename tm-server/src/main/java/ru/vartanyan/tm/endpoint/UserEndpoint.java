package ru.vartanyan.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.vartanyan.tm.api.endpoint.IUserEndpoint;
import ru.vartanyan.tm.api.service.ServiceLocator;
import ru.vartanyan.tm.dto.SessionDTO;
import ru.vartanyan.tm.enumerated.Role;
import ru.vartanyan.tm.dto.UserDTO;
import ru.vartanyan.tm.exception.empty.EmptyLoginException;
import ru.vartanyan.tm.exception.system.AccessDeniedException;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@WebService
public final class UserEndpoint extends AbstractEndpoint implements IUserEndpoint {

    public UserEndpoint(@NotNull final ServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @Override
    @Nullable
    @WebMethod
    public UserDTO findUserOneBySession(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO session
    ) throws AccessDeniedException {
        serviceLocator.getSessionServiceDTO().validate(session);
        if (session == null) throw new AccessDeniedException();
        return serviceLocator.getUserServiceDTO().findOneById(session.getUserId());
    }

    @Override
    @WebMethod
    public void createUser(
            @WebParam(name = "login", partName = "login") @Nullable final String login,
            @WebParam(name = "password", partName = "password") @Nullable final String password,
            @WebParam(name = "email", partName = "email") @Nullable final String email
    ) {
        serviceLocator.getUserServiceDTO().create(login, password, email);
    }

    @WebMethod
    public UserDTO findUserByLogin(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO session,
            @WebParam(name = "login", partName = "login") @Nullable final String login
    ) throws AccessDeniedException {
        serviceLocator.getSessionServiceDTO().validate(session);
        return serviceLocator.getUserServiceDTO().findByLogin(login);
    }

    @Override
    @WebMethod
    public void setPassword(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO session,
            @WebParam(name = "password", partName = "password") @Nullable final String password
    ) throws AccessDeniedException {
        serviceLocator.getSessionServiceDTO().validate(session);
        if (session == null) throw new AccessDeniedException();
        serviceLocator.getUserServiceDTO().setPassword(session.getUserId(), password);
    }

    @Override
    @WebMethod
    public void updateUser(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO session,
            @WebParam(name = "firstName", partName = "firstName") @Nullable final String firstName,
            @WebParam(name = "lastName", partName = "lastName") @Nullable final String lastName,
            @WebParam(name = "middleName", partName = "middleName") @Nullable final String middleName
    ) throws AccessDeniedException, AccessDeniedException {
        serviceLocator.getSessionServiceDTO().validate(session);
        if (session == null) throw new AccessDeniedException();
        serviceLocator.getUserServiceDTO().updateUser(session.getUserId(), firstName, lastName, middleName);
    }

}
