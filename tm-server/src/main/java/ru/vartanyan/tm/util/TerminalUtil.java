package ru.vartanyan.tm.util;

import org.jetbrains.annotations.NotNull;
import ru.vartanyan.tm.exception.incorrect.IsNotNumberException;

import java.util.Scanner;

public interface TerminalUtil {

    @NotNull Scanner SCANNER = new Scanner(System.in);

    static String nextLine() {
        return SCANNER.nextLine();
    }

    static Integer nextNumber() throws Exception {
        @NotNull final String value = nextLine();
        try {
            return Integer.parseInt(value);
        } catch (Exception e) {
            throw new IsNotNumberException(value);
        }
    }

}
