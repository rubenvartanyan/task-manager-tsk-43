package ru.vartanyan.tm.api.repository.model;

import lombok.SneakyThrows;
import org.apache.ibatis.annotations.*;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.vartanyan.tm.api.IRepository;
import ru.vartanyan.tm.dto.TaskDTO;
import ru.vartanyan.tm.enumerated.Status;
import ru.vartanyan.tm.model.Task;

import java.util.List;
import java.util.Optional;

public interface ITaskRepository extends IRepository<Task> {

    void clear();

    void clearByUserId(@NotNull String userId);

    @NotNull
    List<Task> findAll();

    @NotNull
    List<Task> findAllByProjectId(
            @NotNull String userId,
            @NotNull String projectId
    );

    @NotNull
    List<Task> findAllByUserId(@Nullable String userId);

    @NotNull
    Task findOneById(@Nullable String id);

    @NotNull
    Task findOneByIdAndUserId(
            @Nullable String userId, @NotNull String id
    );

    @NotNull
    Task findOneByIndex(
            @Nullable String userId, @NotNull Integer index
    );

    void bindTaskByProjectId(
            @NotNull String userId,
            @NotNull String projectId,
            @NotNull String taskId
    );

    void unbindTaskFromProjectId(@NotNull String userId, @NotNull String id);

    @NotNull
    Task findOneByName(
            @Nullable String userId, @NotNull String name
    );

    void removeAllByProjectId(
            @NotNull String userId,
            @NotNull String projectId
    );

    void removeOneById(@Nullable String id);

    void removeOneByIdAndUserId(@Nullable String userId, @NotNull String id);

    void removeOneByName(
            @Nullable String userId, @NotNull String name
    );

}
