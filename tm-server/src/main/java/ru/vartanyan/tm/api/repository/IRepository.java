package ru.vartanyan.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import ru.vartanyan.tm.model.AbstractEntity;

import javax.persistence.TypedQuery;
import java.util.Optional;

public interface IRepository<E extends AbstractEntity> {

    void add(@NotNull E entity);

    Optional<E> getSingleResult(@NotNull TypedQuery<E> query);

    void update(E entity);

}
