package ru.vartanyan.tm.api.service.model;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.vartanyan.tm.dto.SessionDTO;
import ru.vartanyan.tm.enumerated.Role;
import ru.vartanyan.tm.exception.system.WrongRoleException;
import ru.vartanyan.tm.model.Session;

public interface ISessionService extends IService<Session> {

    @Nullable Session close(@Nullable Session session);

    @Nullable
    Session open(String login, String password);

    void validate(@Nullable Session session);

    void validateAdmin(@Nullable Session session, @Nullable Role role);

    @SneakyThrows
    void remove(@Nullable Session entity);

}
