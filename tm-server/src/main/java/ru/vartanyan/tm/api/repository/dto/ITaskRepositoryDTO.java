package ru.vartanyan.tm.api.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.vartanyan.tm.api.repository.IRepositoryDTO;
import ru.vartanyan.tm.dto.TaskDTO;

import java.util.List;
import java.util.Optional;

public interface ITaskRepositoryDTO extends IRepositoryDTO<TaskDTO> {

    void clear();

    void clearByUserId(@NotNull String userId);

    @NotNull
    List<TaskDTO> findAll();

    @NotNull
    List<TaskDTO> findAllByProjectId(
            @NotNull String userId,
            @NotNull String projectId
    );

    @NotNull
    List<TaskDTO> findAllByUserId(@Nullable String userId);

    @NotNull
    TaskDTO findOneById(@Nullable String id);

    @NotNull
    TaskDTO findOneByIdAndUserId(
            @Nullable String userId,
            @NotNull String id
    );

    @NotNull
    TaskDTO findOneByIndex(
            @Nullable String userId,
            @NotNull Integer index
    );

    void bindTaskByProjectId(
            @NotNull String userId,
            @NotNull String projectId,
            @NotNull String taskId
    );

    void unbindTaskFromProjectId(@NotNull String userId,
                                 @NotNull String id);

    @NotNull
    TaskDTO findOneByName(
            @Nullable String userId,
            @NotNull String name
    );

    void removeAllByProjectId(
            @NotNull String userId,
            @NotNull String projectId
    );

    void removeOneById(@Nullable String id);

    void removeOneByIdAndUserId(@Nullable String userId, @NotNull String id);

    void removeOneByName(
            @Nullable String userId, @NotNull String name
    );

}
