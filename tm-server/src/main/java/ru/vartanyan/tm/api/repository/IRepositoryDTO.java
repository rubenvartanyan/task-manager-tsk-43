package ru.vartanyan.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import ru.vartanyan.tm.dto.AbstractEntityDTO;

import javax.persistence.TypedQuery;
import java.util.Optional;

public interface IRepositoryDTO<E extends AbstractEntityDTO> {

    void add(@NotNull E entity);

    @NotNull E getSingleResult(@NotNull TypedQuery<E> query);

    void update(E entity);

}
