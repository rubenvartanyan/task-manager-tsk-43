package ru.vartanyan.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.vartanyan.tm.dto.SessionDTO;
import ru.vartanyan.tm.dto.TaskDTO;
import ru.vartanyan.tm.exception.system.AccessDeniedException;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import java.util.List;

public interface ITaskEndpoint {

    @NotNull
    @WebMethod
    TaskDTO addTask(
            @WebParam(name = "session", partName = "session") @NotNull SessionDTO session,
            @WebParam(name = "name", partName = "name") @NotNull String name,
            @WebParam(name = "description", partName = "description") @NotNull String description
    ) throws AccessDeniedException;

    @WebMethod
    void bindTaskByProject(
            @WebParam(name = "session", partName = "session") @NotNull SessionDTO session,
            @WebParam(name = "projectId", partName = "projectId") @Nullable String projectId,
            @WebParam(name = "taskId", partName = "taskId") @Nullable String taskId
    ) throws AccessDeniedException;

    @NotNull
    @WebMethod
    List<TaskDTO> findAllByProjectId(
            @WebParam(name = "session", partName = "session") @NotNull SessionDTO session,
            @WebParam(name = "projectId", partName = "projectId") @Nullable String projectId
    ) throws AccessDeniedException;

    @NotNull
    @WebMethod
    List<TaskDTO> findAllTask(
            @WebParam(name = "session", partName = "session") @NotNull SessionDTO session
    ) throws AccessDeniedException;

    @WebMethod
    void clearBySessionTask(
            @WebParam(name = "session", partName = "session") @Nullable SessionDTO session
    ) throws AccessDeniedException;

    @Nullable
    @WebMethod
    TaskDTO findTaskOneById(
            @WebParam(name = "session", partName = "session") @NotNull SessionDTO session,
            @WebParam(name = "id", partName = "id") @NotNull String id
    ) throws AccessDeniedException;

    @Nullable
    @WebMethod
    TaskDTO findTaskOneByIndex(
            @WebParam(name = "session", partName = "session") @NotNull SessionDTO session,
            @WebParam(name = "index", partName = "index") @NotNull Integer index
    ) throws AccessDeniedException;

    @Nullable
    @WebMethod
    TaskDTO findTaskOneByName(
            @WebParam(name = "session", partName = "session") @NotNull SessionDTO session,
            @WebParam(name = "name", partName = "name") @NotNull String name
    ) throws AccessDeniedException;

    void finishTaskById(
            @NotNull SessionDTO session,
            @NotNull String id
    ) throws AccessDeniedException;

    void finishTaskByIndex(
            @NotNull SessionDTO session,
            @NotNull Integer index
    ) throws AccessDeniedException;

    void finishTaskByName(
            @NotNull SessionDTO session,
            @NotNull String name
    ) throws AccessDeniedException;

    @WebMethod
    void removeTask(
            @WebParam(name = "session", partName = "session") @NotNull SessionDTO session,
            @WebParam(name = "task", partName = "task") @NotNull TaskDTO task
    ) throws AccessDeniedException;

    @WebMethod
    void removeTaskOneById(
            @WebParam(name = "session", partName = "session") @NotNull SessionDTO session,
            @WebParam(name = "id", partName = "id") @NotNull String id
    ) throws AccessDeniedException;

    @WebMethod
    void removeTaskOneByIndex(
            @WebParam(name = "session", partName = "session") @NotNull SessionDTO session,
            @WebParam(name = "index", partName = "index") @NotNull Integer index
    ) throws AccessDeniedException;

    @WebMethod
    void removeTaskOneByName(
            @WebParam(name = "session", partName = "session") @NotNull SessionDTO session,
            @WebParam(name = "name", partName = "name") @NotNull String name
    ) throws AccessDeniedException;

    void startTaskById(
            @NotNull SessionDTO session,
            @NotNull String id
    ) throws AccessDeniedException;

    void startTaskByIndex(
            @NotNull SessionDTO session,
            @NotNull Integer index
    ) throws AccessDeniedException;

    void startTaskByName(
            @NotNull SessionDTO session,
            @NotNull String name
    ) throws AccessDeniedException;

    @WebMethod
    void unbindTaskFromProject(
            @WebParam(name = "session", partName = "session") @NotNull SessionDTO session,
            @WebParam(name = "taskId", partName = "taskId") @Nullable String taskId
    ) throws AccessDeniedException;

    void updateTaskById(
            @NotNull SessionDTO session,
            @NotNull String id,
            @NotNull String name,
            @NotNull String description
    ) throws AccessDeniedException;

    void updateTaskByIndex(
            @NotNull SessionDTO session,
            @NotNull Integer index,
            @NotNull String name,
            @NotNull String description
    ) throws AccessDeniedException;

}
