package ru.vartanyan.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.vartanyan.tm.dto.SessionDTO;
import ru.vartanyan.tm.dto.UserDTO;
import ru.vartanyan.tm.exception.empty.EmptyEmailException;
import ru.vartanyan.tm.exception.empty.EmptyLoginException;
import ru.vartanyan.tm.exception.empty.EmptyPasswordException;
import ru.vartanyan.tm.exception.system.AccessDeniedException;

import javax.jws.WebMethod;
import javax.jws.WebParam;

public interface IUserEndpoint {

    @WebMethod
    void createUser(
            @WebParam(name = "login", partName = "login") @Nullable String login,
            @WebParam(name = "password", partName = "password") @Nullable String password,
            @WebParam(name = "email", partName = "email") @Nullable String email
    ) throws AccessDeniedException, EmptyLoginException, EmptyPasswordException, EmptyEmailException;

    @WebMethod
    UserDTO findUserByLogin(
            @WebParam(name = "session", partName = "session") @NotNull SessionDTO session,
            @WebParam(name = "login", partName = "login") @Nullable String login
    ) throws AccessDeniedException;

    @Nullable
    @WebMethod
    UserDTO findUserOneBySession(
            @WebParam(name = "session", partName = "session") @NotNull SessionDTO session
    ) throws AccessDeniedException;

    @WebMethod
    void setPassword(
            @WebParam(name = "session", partName = "session") @NotNull SessionDTO session,
            @WebParam(name = "password", partName = "password") @Nullable String password
    ) throws AccessDeniedException;

    @WebMethod
    void updateUser(
            @WebParam(name = "session", partName = "session") @NotNull SessionDTO session,
            @WebParam(name = "firstName", partName = "firstName") @Nullable String firstName,
            @WebParam(name = "lastName", partName = "lastName") @Nullable String lastName,
            @WebParam(name = "middleName", partName = "middleName") @Nullable String middleName
    ) throws AccessDeniedException;

}
