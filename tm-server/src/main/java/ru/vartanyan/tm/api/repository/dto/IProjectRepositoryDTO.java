package ru.vartanyan.tm.api.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.vartanyan.tm.api.repository.IRepositoryDTO;
import ru.vartanyan.tm.dto.ProjectDTO;

import java.util.List;
import java.util.Optional;

public interface IProjectRepositoryDTO extends IRepositoryDTO<ProjectDTO> {

    void clear();

    void clearByUserId(@NotNull String userId);

    @NotNull
    List<ProjectDTO> findAll();

    @NotNull
    List<ProjectDTO> findAllByUserId(@Nullable String userId);

    @NotNull
    ProjectDTO findOneById(@Nullable String id);

    @NotNull
    ProjectDTO findOneByIdAndUserId(
            @Nullable String userId,
            @NotNull String id
    );

    @NotNull
    ProjectDTO findOneByIndex(
            @Nullable String userId,
            @NotNull Integer index
    );

    ProjectDTO findOneByName(
            @Nullable String userId,
            @NotNull String name
    );

    void removeOneById(@Nullable String id);

    void removeOneByIdAndUserId(@Nullable String userId,
                                @NotNull String id);

    void removeOneByName(
            @Nullable String userId,
            @NotNull String name
    );

}

