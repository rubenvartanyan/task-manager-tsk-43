package ru.vartanyan.tm.api.repository.model;

import org.apache.ibatis.annotations.*;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.vartanyan.tm.api.IRepository;
import ru.vartanyan.tm.dto.SessionDTO;
import ru.vartanyan.tm.model.Session;

import java.util.List;
import java.util.Optional;

public interface ISessionRepository extends IRepository<ru.vartanyan.tm.model.Session> {

    void clear();

    @NotNull
    List<ru.vartanyan.tm.model.Session> findAll();

    @NotNull
    Session findOneById(@Nullable String id);

    void removeOneById(@Nullable String id);

}
