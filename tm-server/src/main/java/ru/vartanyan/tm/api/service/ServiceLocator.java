package ru.vartanyan.tm.api.service;

import org.jetbrains.annotations.NotNull;
import ru.vartanyan.tm.api.IPropertyService;
import ru.vartanyan.tm.api.service.dto.*;
import ru.vartanyan.tm.api.service.model.*;

public interface ServiceLocator {

    @NotNull
    IProjectService getProjectService();

    @NotNull
    IProjectTaskService getProjectTaskService();

    @NotNull
    IPropertyService getPropertyService();

    @NotNull
    ISessionService getSessionService();

    @NotNull
    ITaskService getTaskService();

    @NotNull
    IUserService getUserService();

    @NotNull
    IProjectServiceDTO getProjectServiceDTO();

    @NotNull
    IProjectTaskServiceDTO getProjectTaskServiceDTO();

    @NotNull
    ISessionServiceDTO getSessionServiceDTO();

    @NotNull
    ITaskServiceDTO getTaskServiceDTO();

    @NotNull
    IUserServiceDTO getUserServiceDTO();

}
