package ru.vartanyan.tm.api.repository.model;

import lombok.SneakyThrows;
import org.apache.ibatis.annotations.*;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.vartanyan.tm.api.IRepository;
import ru.vartanyan.tm.enumerated.Status;
import ru.vartanyan.tm.dto.ProjectDTO;
import ru.vartanyan.tm.model.Project;

import java.util.List;
import java.util.Optional;

public interface IProjectRepository extends IRepository<Project> {

    void clear();

    void clearByUserId(@NotNull String userId);

    @NotNull
    List<Project> findAll();

    @NotNull
    List<Project> findAllByUserId(@Nullable String userId);

    @NotNull
    Project findOneById(@Nullable String id);

    @NotNull
    Project findOneByIdAndUserId(
            @Nullable String userId,
            @NotNull String id
    );

    @NotNull
    Project findOneByIndex(
            @Nullable String userId,
            @NotNull Integer index
    );

    Project findOneByName(
            @Nullable String userId,
            @NotNull String name
    );

    void removeOneById(@Nullable String id);

    void removeOneByIdAndUserId(@Nullable String userId, @NotNull String id);

    void removeOneByName(
            @Nullable String userId, @NotNull String name
    );

}

