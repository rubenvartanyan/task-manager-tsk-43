package ru.vartanyan.tm.api.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.vartanyan.tm.api.repository.IRepositoryDTO;
import ru.vartanyan.tm.dto.SessionDTO;

import java.util.List;
import java.util.Optional;

public interface ISessionRepositoryDTO extends IRepositoryDTO<SessionDTO> {

    void clear();

    @NotNull
    List<SessionDTO> findAll();

    @NotNull
    SessionDTO findOneById(@Nullable String id);

    void removeOneById(@Nullable String id);

}
