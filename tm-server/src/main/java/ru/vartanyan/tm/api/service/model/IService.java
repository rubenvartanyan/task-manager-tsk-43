package ru.vartanyan.tm.api.service.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.vartanyan.tm.api.IRepository;
import ru.vartanyan.tm.exception.empty.EmptyIdException;
import ru.vartanyan.tm.dto.AbstractEntityDTO;
import ru.vartanyan.tm.exception.system.NullObjectException;
import ru.vartanyan.tm.model.AbstractEntity;

import java.sql.SQLException;
import java.util.List;
import java.util.Optional;

public interface IService<E extends AbstractEntity> {

    void add(@NotNull E entity) throws NullObjectException;

    void addAll(@NotNull List<E> entities);

    void clear();

    @NotNull
    List<E> findAll();

    @NotNull
    E findOneById(@Nullable String id);

    void removeOneById(@Nullable String id);

}

