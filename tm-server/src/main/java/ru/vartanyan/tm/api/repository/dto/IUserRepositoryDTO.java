package ru.vartanyan.tm.api.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.vartanyan.tm.api.repository.IRepositoryDTO;
import ru.vartanyan.tm.dto.UserDTO;

import java.util.List;

public interface IUserRepositoryDTO extends IRepositoryDTO<UserDTO> {

    void clear();

    @NotNull
    List<UserDTO> findAll();

    @NotNull
    UserDTO findByLogin(@NotNull String login);

    @NotNull
    UserDTO findOneById(@Nullable String id);

    void removeByLogin(@NotNull String login);

    void removeOneById(@Nullable String id);

}
