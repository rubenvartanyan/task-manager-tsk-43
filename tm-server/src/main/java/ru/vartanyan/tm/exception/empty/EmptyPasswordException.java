package ru.vartanyan.tm.exception.empty;

public class EmptyPasswordException extends Exception{

    public EmptyPasswordException() {
        super("Error! Login cannot be null or empty...");
    }

}
