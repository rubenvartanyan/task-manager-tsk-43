package ru.vartanyan.tm.bootstrap;

import lombok.Getter;
import lombok.SneakyThrows;
import org.jetbrains.annotations.Nullable;
import org.jetbrains.annotations.NotNull;
import ru.vartanyan.tm.api.IPropertyService;
import ru.vartanyan.tm.api.endpoint.*;
import ru.vartanyan.tm.api.service.*;

import ru.vartanyan.tm.api.service.dto.*;
import ru.vartanyan.tm.api.service.model.*;
import ru.vartanyan.tm.endpoint.*;
import ru.vartanyan.tm.enumerated.Role;
import ru.vartanyan.tm.dto.SessionDTO;
import ru.vartanyan.tm.service.*;
import ru.vartanyan.tm.service.dto.*;
import ru.vartanyan.tm.service.model.*;
import ru.vartanyan.tm.util.SystemUtil;

import javax.xml.ws.Endpoint;
import java.io.File;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;

@Getter
public final class Bootstrap implements ServiceLocator{

    @NotNull
    public final IPropertyService propertyService = new PropertyService();

    @NotNull
    public final IConnectionService connectionService = new ConnectionService(propertyService);

    @NotNull
    public final ILoggerService loggerService = new LoggerService();

    @NotNull
    public final IProjectServiceDTO projectServiceDTO = new ProjectServiceDTO(connectionService);

    @NotNull
    public final IProjectTaskServiceDTO projectTaskServiceDTO = new ProjectTaskServiceDTO(connectionService);

    @NotNull
    public final ISessionServiceDTO sessionServiceDTO = new SessionServiceDTO(connectionService, this);

    @NotNull
    public final ITaskServiceDTO taskServiceDTO = new TaskServiceDTO(connectionService);

    @NotNull
    public final IUserServiceDTO userServiceDTO = new UserServiceDTO(propertyService, connectionService);

    @NotNull
    public final IProjectService projectService = new ProjectService(connectionService);

    @NotNull
    public final IProjectTaskService projectTaskService = new ProjectTaskService(connectionService);

    @NotNull
    public final ISessionService sessionService = new SessionService(connectionService, this);

    @NotNull
    public final ITaskService taskService = new TaskService(connectionService);

    @NotNull
    public final IUserService userService = new UserService(propertyService, connectionService);

    @NotNull
    public final IAdminEndpoint adminEndpoint = new AdminEndpoint(this);

    @NotNull
    public final IProjectEndpoint projectEndpoint = new ProjectEndpoint(this);

    @NotNull
    public final ISessionEndpoint sessionEndpoint = new SessionEndpoint(this);

    @NotNull
    public final ITaskEndpoint taskEndpoint = new TaskEndpoint(this);

    @NotNull
    public final IUserEndpoint userEndpoint = new UserEndpoint(this);

    @Nullable
    private SessionDTO session = null;
    public Bootstrap() {
    }

    @SneakyThrows
    public void initPID() {
        final String fileName = "task-manager.pid";
        final String pid = Long.toString(SystemUtil.getPID());
        Files.write(Paths.get(fileName), pid.getBytes(StandardCharsets.UTF_8));
        final File file = new File(fileName);
        file.deleteOnExit();
    }

    private void registry(@NotNull final Object endpoint) {
        @NotNull final String host = propertyService.getServerHost();
        @NotNull final String port = propertyService.getServerPort();
        @NotNull final String name = endpoint.getClass().getSimpleName();
        @NotNull final String wsdl = "http://" + host + ":" + port + "/" + name + "?wsdl";
        System.out.println(wsdl);
        Endpoint.publish(wsdl, endpoint);
    }

    private void initEndpoint() {
        registry(sessionEndpoint);
        registry(taskEndpoint);
        registry(projectEndpoint);
        registry(userEndpoint);
        registry(adminEndpoint);
    }

    public void init() throws Exception {
        initPID();
        initEndpoint();
        initUser();
    }

    public void initUser() throws Exception {
        userService.create("test", "test");
        userService.create("admin", "admin", Role.ADMIN);
    }

}
