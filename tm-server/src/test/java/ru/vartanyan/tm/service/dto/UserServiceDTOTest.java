package ru.vartanyan.tm.service.dto;

import org.jetbrains.annotations.NotNull;
import org.junit.Assert;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.vartanyan.tm.api.IPropertyService;
import ru.vartanyan.tm.api.service.IConnectionService;
import ru.vartanyan.tm.api.service.dto.IUserServiceDTO;
import ru.vartanyan.tm.api.service.model.IUserService;
import ru.vartanyan.tm.dto.UserDTO;
import ru.vartanyan.tm.exception.system.NullObjectException;
import ru.vartanyan.tm.marker.DBCategory;
import ru.vartanyan.tm.model.User;
import ru.vartanyan.tm.service.ConnectionService;
import ru.vartanyan.tm.service.PropertyService;
import ru.vartanyan.tm.service.model.UserService;

import java.util.ArrayList;
import java.util.List;

public class UserServiceDTOTest {

    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    @NotNull
    private final IConnectionService connectionService = new ConnectionService(propertyService);

    @NotNull
    private final IUserServiceDTO userService = new UserServiceDTO(propertyService, connectionService);

    @Test
    @Category(DBCategory.class)
    public void addAllTest() {
        final List<UserDTO> users = new ArrayList<>();
        final UserDTO user1 = new UserDTO();
        final UserDTO user2 = new UserDTO();
        users.add(user1);
        users.add(user2);
        userService.addAll(users);
        Assert.assertTrue(userService.findOneById(user1.getId()) != null);
        Assert.assertTrue(userService.findOneById(user2.getId()) != null);
        userService.remove(users.get(0));
        userService.remove(users.get(1));
    }

    @Test
    @Category(DBCategory.class)
    public void addTest() {
        final UserDTO user = new UserDTO();
        userService.add(user);
        Assert.assertNotNull(userService.findOneById(user.getId()));
        userService.remove(user);
    }

    @Test
    @Category(DBCategory.class)
    public void findAll() {
        final int userSize = userService.findAll().size();
        userService.create("testFindAll", "test", "-");
        Assert.assertEquals(userSize + 1, userService.findAll().size());
        userService.removeByLogin("testFindAll");
    }

    @Test
    @Category(DBCategory.class)
    public void findByLogin() {
        final UserDTO user = new UserDTO();
        user.setLogin("testFindL");
        userService.add(user);
        final String login = user.getLogin();
        Assert.assertNotNull(login);
        Assert.assertTrue(userService.findByLogin(login) != null);
        userService.removeByLogin("testFindL");
    }

    @Test
    @Category(DBCategory.class)
    public void findOneByIdTest() {
        final UserDTO user = new UserDTO();
        final String userId = user.getId();
        userService.add(user);
        Assert.assertNotNull(userService.findOneById(userId));
        userService.remove(user);
    }

    @Test
    @Category(DBCategory.class)
    public void findOneByIndexTest() {
        final UserDTO user = new UserDTO();
        userService.add(user);
        final String userId = user.getId();
        Assert.assertTrue(userService.findOneById(userId) != null);
        userService.remove(user);
    }

    @Test
    @Category(DBCategory.class)
    public void isLoginExist() {
        final UserDTO user = new UserDTO();
        user.setLogin("testExist");
        userService.add(user);
        final String login = user.getLogin();
        Assert.assertNotNull(login);
        userService.remove(user);
    }

    @Test
    @Category(DBCategory.class)
    public void removeByLogin() {
        final UserDTO user = new UserDTO();
        user.setLogin("testRemoveByLogin");
        userService.add(user);
        final String login = user.getLogin();
        Assert.assertNotNull(login);
        userService.removeByLogin(login);
    }

    @Test
    @Category(DBCategory.class)
    public void removeOneByIdTest() {
        final UserDTO user = new UserDTO();
        userService.add(user);
        final String userId = user.getId();
        userService.removeOneById(userId);
        Assert.assertFalse(userService.findOneById(userId) != null);
    }

    @Test
    @Category(DBCategory.class)
    public void removeTest() {
        final UserDTO user = new UserDTO();
        userService.add(user);
        userService.remove(user);
        Assert.assertNotNull(userService.findOneById(user.getId()));
    }
}
