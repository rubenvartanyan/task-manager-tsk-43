package ru.vartanyan.tm.service.dto;

import org.jetbrains.annotations.NotNull;
import org.junit.Assert;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.vartanyan.tm.api.IPropertyService;
import ru.vartanyan.tm.api.service.IConnectionService;
import ru.vartanyan.tm.api.service.dto.IProjectServiceDTO;
import ru.vartanyan.tm.api.service.dto.IProjectTaskServiceDTO;
import ru.vartanyan.tm.api.service.dto.ITaskServiceDTO;
import ru.vartanyan.tm.api.service.dto.IUserServiceDTO;
import ru.vartanyan.tm.api.service.model.IProjectService;
import ru.vartanyan.tm.api.service.model.IProjectTaskService;
import ru.vartanyan.tm.api.service.model.ITaskService;
import ru.vartanyan.tm.api.service.model.IUserService;
import ru.vartanyan.tm.dto.ProjectDTO;
import ru.vartanyan.tm.dto.TaskDTO;
import ru.vartanyan.tm.dto.UserDTO;
import ru.vartanyan.tm.exception.system.NullObjectException;
import ru.vartanyan.tm.marker.DBCategory;
import ru.vartanyan.tm.model.Project;
import ru.vartanyan.tm.model.Task;
import ru.vartanyan.tm.model.User;
import ru.vartanyan.tm.service.ConnectionService;
import ru.vartanyan.tm.service.PropertyService;
import ru.vartanyan.tm.service.model.ProjectService;
import ru.vartanyan.tm.service.model.ProjectTaskService;
import ru.vartanyan.tm.service.model.TaskService;
import ru.vartanyan.tm.service.model.UserService;

import java.util.Optional;

public class ProjectTaskServiceDTOTest {


    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    @NotNull
    private final IConnectionService connectionService = new ConnectionService(propertyService);

    @NotNull
    private final IProjectTaskServiceDTO projectTaskService = new ProjectTaskServiceDTO(connectionService);

    @NotNull
    private final ITaskServiceDTO taskService = new TaskServiceDTO(connectionService);

    @NotNull
    private final IProjectServiceDTO projectService = new ProjectServiceDTO(connectionService);

    @NotNull
    private final IUserServiceDTO userService = new UserServiceDTO(propertyService, connectionService);

    @Test
    @Category(DBCategory.class)
    public void bindTaskByProjectIdTest() {
        final TaskDTO task = new TaskDTO();
        final @NotNull UserDTO user = userService.findByLogin("test");
        final String userId = user.getId();
        final ProjectDTO project = projectService.add(userId, "testBind", "-");
        final String projectId = project.getId();
        final String taskId = task.getId();
        task.setUserId(userId);
        taskService.add(task);
        projectTaskService.bindTaskByProject(userId, projectId, taskId);
        Assert.assertTrue(!(taskService.findOneById(userId, taskId) == null));
    }

    @Test
    @Category(DBCategory.class)
    public void findAllByProjectIdTest() {
        final TaskDTO task = new TaskDTO();
        final @NotNull UserDTO user = userService.findByLogin("test");
        final String userId = user.getId();
        final ProjectDTO project = projectService.add(userId, "testFindAll", "-");
        final String projectId = project.getId();
        task.setUserId(userId);
        task.setProjectId(projectId);
        taskService.add(task);
        Assert.assertFalse(projectTaskService.findAllByProjectId(userId, projectId).isEmpty());
        Assert.assertEquals(1, projectTaskService.findAllByProjectId(userId, projectId).size());

        final TaskDTO task2 = new TaskDTO();
        task2.setUserId(userId);
        task2.setProjectId(projectId);
        taskService.add(task2);
        Assert.assertEquals(2, projectTaskService.findAllByProjectId(userId, projectId).size());

        final TaskDTO task3 = new TaskDTO();
        final @NotNull UserDTO user2 = userService.findByLogin("test2");
        final String user2Id = user2.getId();
        task3.setUserId(user2Id);
        task3.setProjectId(projectId);
        taskService.add(task3);
        Assert.assertEquals(2, projectTaskService.findAllByProjectId(userId, projectId).size());
        Assert.assertEquals(1, projectTaskService.findAllByProjectId(user2Id, projectId).size());

        final TaskDTO task4 = new TaskDTO();
        final ProjectDTO project2 = projectService.add(userId, "testFindAll2", "-");
        final String project2Id = project2.getId();
        task4.setUserId(userId);
        task4.setProjectId(project2Id);
        taskService.add(task4);
        Assert.assertEquals(2, projectTaskService.findAllByProjectId(userId, projectId).size());
        Assert.assertEquals(1, projectTaskService.findAllByProjectId(userId, project2Id).size());
    }

    @Test
    @Category(DBCategory.class)
    public void removeAllByProjectIdTest() {
        final TaskDTO task = new TaskDTO();
        final TaskDTO task2 = new TaskDTO();
        final TaskDTO task3 = new TaskDTO();
        final @NotNull UserDTO user = userService.findByLogin("test");
        final String userId = user.getId();
        final ProjectDTO project = projectService.add(userId, "testBind", "-");
        final String projectId = project.getId();
        task.setUserId(userId);
        task.setProjectId(projectId);
        task2.setUserId(userId);
        task2.setProjectId(projectId);
        task3.setUserId(userId);
        task3.setProjectId(projectId);
        taskService.add(task);
        taskService.add(task2);
        taskService.add(task3);
        Assert.assertEquals(3, projectTaskService.findAllByProjectId(userId, projectId).size());
        projectTaskService.removeProjectById(userId, projectId);
        Assert.assertTrue(projectTaskService.findAllByProjectId(userId, projectId).isEmpty());
    }

    @Test
    @Category(DBCategory.class)
    public void unbindTaskFromProjectIdTest() {
        final TaskDTO task = new TaskDTO();
        final @NotNull UserDTO user = userService.findByLogin("test");
        final String userId = user.getId();
        final String taskId = task.getId();
        task.setUserId(userId);
        taskService.add(task);
        projectTaskService.unbindTaskFromProject(userId, taskId);
        Assert.assertTrue(!(taskService.findOneById(userId, taskId) == null));
        projectTaskService.unbindTaskFromProject(userId, taskId);
        final TaskDTO task2 = taskService.findOneById(userId, taskId);
        Assert.assertNull(task2.getProjectId());
    }

}
