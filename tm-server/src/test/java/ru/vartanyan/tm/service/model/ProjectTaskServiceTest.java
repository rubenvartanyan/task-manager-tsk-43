package ru.vartanyan.tm.service.model;

import org.jetbrains.annotations.NotNull;
import org.junit.Assert;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.vartanyan.tm.api.IPropertyService;
import ru.vartanyan.tm.api.service.IConnectionService;
import ru.vartanyan.tm.api.service.model.IProjectService;
import ru.vartanyan.tm.api.service.model.IProjectTaskService;
import ru.vartanyan.tm.api.service.model.ITaskService;
import ru.vartanyan.tm.api.service.model.IUserService;
import ru.vartanyan.tm.dto.ProjectDTO;
import ru.vartanyan.tm.dto.TaskDTO;
import ru.vartanyan.tm.exception.system.NullObjectException;
import ru.vartanyan.tm.marker.DBCategory;
import ru.vartanyan.tm.dto.UserDTO;
import ru.vartanyan.tm.model.Project;
import ru.vartanyan.tm.model.Task;
import ru.vartanyan.tm.model.User;
import ru.vartanyan.tm.service.ConnectionService;
import ru.vartanyan.tm.service.PropertyService;
import ru.vartanyan.tm.service.model.ProjectTaskService;
import ru.vartanyan.tm.service.model.TaskService;

import java.util.List;
import java.util.Optional;

public class ProjectTaskServiceTest {

    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    @NotNull
    private final IConnectionService connectionService = new ConnectionService(propertyService);

    @NotNull
    private final IProjectTaskService projectTaskService = new ProjectTaskService(connectionService);

    @NotNull
    private final ITaskService taskService = new TaskService(connectionService);

    @NotNull
    private final IProjectService projectService = new ProjectService(connectionService);

    @NotNull
    private final IUserService userService = new UserService(propertyService, connectionService);

    @Test
    @Category(DBCategory.class)
    public void bindTaskByProjectIdTest() throws NullObjectException {
        final Task task = new Task();
        final @NotNull User user = userService.findByLogin("test");
        final String userId = user.getId();
        final Project project = projectService.add(userId, "testBind", "-");
        final String projectId = project.getId();
        final String taskId = task.getId();
        task.setUser(user);
        taskService.add(task);
        projectTaskService.bindTaskByProject(userId, projectId, taskId);
        Assert.assertTrue(taskService.findOneById(userId, taskId) == null);
    }

    @Test
    @Category(DBCategory.class)
    public void findAllByProjectIdTest() throws NullObjectException {
        final Task task = new Task();
        final @NotNull User user = userService.findByLogin("test");
        final String userId = user.getId();
        final Project project = projectService.add(userId, "testFindAll", "-");
        final String projectId = project.getId();
        task.setUser(user);
        task.setProject(project);
        taskService.add(task);
        Assert.assertFalse(projectTaskService.findAllByProjectId(userId, projectId).isEmpty());
        Assert.assertEquals(1, projectTaskService.findAllByProjectId(userId, projectId).size());

        final Task task2 = new Task();
        task2.setUser(user);
        task2.setProject(project);
        taskService.add(task2);
        Assert.assertEquals(2, projectTaskService.findAllByProjectId(userId, projectId).size());

        final Task task3 = new Task();
        final @NotNull User user2 = userService.findByLogin("test2");
        final String user2Id = user2.getId();
        task3.setUser(user2);
        task3.setProject(project);
        taskService.add(task3);
        Assert.assertEquals(2, projectTaskService.findAllByProjectId(userId, projectId).size());
        Assert.assertEquals(1, projectTaskService.findAllByProjectId(user2Id, projectId).size());

        final Task task4 = new Task();
        final Project project2 = projectService.add(userId, "testFindAll2", "-");
        final String project2Id = project2.getId();
        task4.setUser(user);
        task4.setProject(project2);
        taskService.add(task4);
        Assert.assertEquals(2, projectTaskService.findAllByProjectId(userId, projectId).size());
        Assert.assertEquals(1, projectTaskService.findAllByProjectId(userId, project2Id).size());
    }

    @Test
    @Category(DBCategory.class)
    public void removeAllByProjectIdTest() throws NullObjectException {
        final Task task = new Task();
        final Task task2 = new Task();
        final Task task3 = new Task();
        final @NotNull User user = userService.findByLogin("test");
        final String userId = user.getId();
        final Project project = projectService.add(userId, "testBind", "-");
        final String projectId = project.getId();
        task.setUser(user);
        task.setProject(project);
        task2.setUser(user);
        task2.setProject(project);
        task3.setUser(user);
        task3.setProject(project);
        taskService.add(task);
        taskService.add(task2);
        taskService.add(task3);
        Assert.assertEquals(3, projectTaskService.findAllByProjectId(userId, projectId).size());
        projectTaskService.removeProjectById(userId, projectId);
        Assert.assertTrue(projectTaskService.findAllByProjectId(userId, projectId).isEmpty());
    }

    @Test
    @Category(DBCategory.class)
    public void unbindTaskFromProjectIdTest() throws NullObjectException {
        final Task task = new Task();
        final @NotNull User user = userService.findByLogin("test");
        final String userId = user.getId();
        final String taskId = task.getId();
        task.setUser(user);
        taskService.add(task);
        projectTaskService.unbindTaskFromProject(userId, taskId);
        Assert.assertTrue(taskService.findOneById(userId, taskId) == null);
        projectTaskService.unbindTaskFromProject(userId, taskId);
        final Task task2 = taskService.findOneById(userId, taskId);
        Assert.assertNull(task2.getProject());
    }

}
