package ru.vartanyan.tm.command.auth;

import org.jetbrains.annotations.NotNull;
import ru.vartanyan.tm.command.AbstractAuthCommand;
import ru.vartanyan.tm.endpoint.Session;
import ru.vartanyan.tm.endpoint.SessionDTO;
import ru.vartanyan.tm.exception.system.AccessDeniedException;
import ru.vartanyan.tm.exception.system.NullObjectException;
import ru.vartanyan.tm.util.TerminalUtil;

public class AuthLoginCommand extends AbstractAuthCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public @NotNull String name() {
        return "login";
    }

    @Override
    public String description() {
        return "Login";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[LOGIN]");
        System.out.println("ENTER LOGIN:");
        @NotNull final String login = TerminalUtil.nextLine();
        System.out.println("ENTER PASSWORD:");
        @NotNull final String password = TerminalUtil.nextLine();
        SessionDTO session = endpointLocator.getSessionEndpoint().openSession(login, password);
        if (session == null) throw new AccessDeniedException();
        bootstrap.setSession(session);
    }

}
