package ru.vartanyan.tm.bootstrap;
import lombok.Getter;
import lombok.Setter;
import lombok.SneakyThrows;
import org.jetbrains.annotations.Nullable;
import org.jetbrains.annotations.NotNull;
import org.reflections.Reflections;
import ru.vartanyan.tm.api.IPropertyService;
import ru.vartanyan.tm.api.endpoint.EndpointLocator;
import ru.vartanyan.tm.api.repository.*;
import ru.vartanyan.tm.api.service.*;
import ru.vartanyan.tm.command.AbstractCommand;
import ru.vartanyan.tm.command.system.ExitCommand;
import ru.vartanyan.tm.component.FileScanner;
import ru.vartanyan.tm.endpoint.*;
import ru.vartanyan.tm.repository.*;
import ru.vartanyan.tm.service.*;
import ru.vartanyan.tm.util.SystemUtil;
import ru.vartanyan.tm.util.TerminalUtil;
import sun.rmi.transport.Endpoint;

import java.io.File;
import java.lang.Exception;
import java.lang.reflect.Modifier;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import java.util.Set;

@Setter
@Getter
public final class Bootstrap implements ServiceLocator, EndpointLocator {

    @NotNull
    public final ICommandRepository commandRepository = new CommandRepository();

    @NotNull
    public final ICommandService commandService = new CommandService(commandRepository);

    @NotNull
    public final ILoggerService loggerService = new LoggerService();

    @NotNull
    public final IPropertyService propertyService = new PropertyService();

    @NotNull
    private final FileScanner fileScanner = new FileScanner(this);

    @NotNull
    public final AdminEndpointService adminEndpointService = new AdminEndpointService();

    @NotNull
    public final AdminEndpoint adminEndpoint = adminEndpointService.getAdminEndpointPort();

    @NotNull
    public final ProjectEndpointService projectEndpointService = new ProjectEndpointService();

    @NotNull
    public final ProjectEndpoint projectEndpoint = projectEndpointService.getProjectEndpointPort();

    @NotNull
    public final SessionEndpointService sessionEndpointService = new SessionEndpointService();

    @NotNull
    public final SessionEndpoint sessionEndpoint = sessionEndpointService.getSessionEndpointPort();

    @NotNull
    public final TaskEndpointService taskEndpointService = new TaskEndpointService();

    @NotNull
    public final TaskEndpoint taskEndpoint = taskEndpointService.getTaskEndpointPort();

    @NotNull
    public final UserEndpointService userEndpointService = new UserEndpointService();

    @NotNull
    public final UserEndpoint userEndpoint = userEndpointService.getUserEndpointPort();

    @Nullable
    private SessionDTO session = null;

    private void initFileScanner() {
        fileScanner.init();
    }

    @SneakyThrows
    public void initPID() {
        final String fileName = "task-manager.pid";
        final String pid = Long.toString(SystemUtil.getPID());
        Files.write(Paths.get(fileName), pid.getBytes(StandardCharsets.UTF_8));
        final File file = new File(fileName);
        file.deleteOnExit();
    }

    @SneakyThrows
    private void initCommands() {
        @NotNull final Reflections reflections = new Reflections("ru.vartanyan.tm.command");
        @NotNull final Set<Class<? extends AbstractCommand>> classes =
                reflections.getSubTypesOf(ru.vartanyan.tm.command.AbstractCommand.class);
        for (@NotNull final Class<? extends AbstractCommand> clazz : classes) {
            final boolean isAbstract = Modifier.isAbstract(clazz.getModifiers());
            if (isAbstract) continue;
            registry(clazz.newInstance());
        }
    }

    public void process() {
        while (true){
            System.out.println();
            System.out.println("ENTER COMMAND:");
            @NotNull final String command = TerminalUtil.nextLine();
            loggerService.command(command);
            try {
                parseCommand(command);
                System.out.println("[OK]");
            } catch (final Exception e) {
                loggerService.error(e);
                System.err.println("[FAIL]");
            }
        }
    }

    public boolean parseArgs(@Nullable String[] args) throws Exception {
        if (args == null || args.length == 0) return false;
        @NotNull final String arg = args[0];
        parseArg(arg);
        return true;
    }

    private void parseArg(@Nullable final String arg) throws Exception {
        if (arg == null || arg.isEmpty()) return;
        @NotNull final AbstractCommand command = commandService.getCommandByName(arg);
        if (command == null) showIncorrectArg();
        else command.execute();
    }

    public void showIncorrectArg() {
        System.out.println("Error! Argument not found...");
    }

    public void parseCommand(@Nullable final String cmd) throws Exception {
        if (cmd == null || cmd.isEmpty()) return;
        @Nullable final AbstractCommand command = commandService.getCommandByName(cmd);
        if (command == null) {
            showIncorrectCommand();
            return;
        }
        command.execute();
    }

    private void registry(@Nullable final AbstractCommand command) {
        if (command == null) return;
        command.setServiceLocator(this);
        command.setEndpointLocator(this);
        command.setBootstrap(this);
        commandService.add(command);
    }

    private void init() {
        initPID();
        initCommands();
        initFileScanner();
    }

    public static void showIncorrectCommand() {
        System.out.println("Error! Command not found...");
    }

    public void run(final String... args) throws Exception {
        init();
        if (parseArgs(args)) new ExitCommand().execute();
        process();
    }

}
